pub fn is_leap_year(year: u64) -> bool {
    let year_has_factor = |b| year % b == 0;

    year_has_factor(4) && (!year_has_factor(100) || year_has_factor(400))
}
